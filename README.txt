# multilink_redirect_bypass

This very specific module can be used to bypass automatic redirects, made by 
the multilink_redirect module, based on the remote IP or User-Agent string.


## Use case

Consider a multilingual site with automatic machine translation using the 
lingotek (https://www.drupal.org/project/lingotek) module. Site internal 
hyperlinks like "/de/hallo-welt", placed in the hand written content, cannot 
be automatically transformed to link their corresponding translation 
"/en/hello-world" (or "/en/node/123"). multilink_redirect 
(https://www.drupal.org/project/multilink) is able to detect the accidental 
language change, intercepts page loading and redirects the user to the 
appropriate page.

In our case, this process disturbs the stateless crawler, we use to index our 
site, by redirecting all requests to pages in a foreign language to the default 
language. This modules solves the problem by dynamically assigning the 'bypass 
multilink redirect' and 'bypass multilink redirect silent' permission, based on 
the remote IP or User-Agent.


## Dependency

* multilink_redirect (which is part of the multilink module)


## Installation

Simply copy this folder into one of Drupal's modules paths (such as 
'sites/all/modules') and activate it using the administration interface
or Drupal's command line tool drush.


## Credits

This module was written by Yannic Labonte <labonte@publicplan.de> due to the 
EA NRW project (http://www.nrw-ea.de) for the publicplan GmbH.

