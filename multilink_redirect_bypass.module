<?php
/**
 * @file
 * Main module file.
 */

/**
 * Implements hook_menu().
 */
function multilink_redirect_bypass_menu() {
  $items = array(
    'admin/config/regional/multilink_redirect/bypass' => array(
      'title' => 'Bypass',
      'description' => 'Configure conditional bypassing of MultiLink Redirect',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('multilink_redirect_bypass_admin'),
      'access arguments' => array('administer multilink redirect'),
      'type' => MENU_NORMAL_ITEM | MENU_LOCAL_TASK,
    ),
  );

  return $items;
}

/**
 * Callback for drupal_get_form().
 *
 * This callback returns the structure of this module's configuration form.
 */
function multilink_redirect_bypass_admin() {
  $form = array();

  $form['multilink_redirect_bypass_by_user_agent'] = array(
    '#type' => 'textarea',
    '#title' => t('User-Agents to ignore'),
    '#default_value' => variable_get('multilink_redirect_bypass_by_user_agent'),
    '#description' => t('Here you can define any user-agent strings which should never be redirected. Enter one path per line.'),
  );

  $form['multilink_redirect_bypass_by_ip'] = array(
    '#type' => 'textarea',
    '#title' => t('IPs to ignore'),
    '#default_value' => variable_get('multilink_redirect_bypass_by_ip'),
    '#description' => t('Here you can define any IPs which should never be redirected. Enter one IP per line.'),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_boot().
 */
function multilink_redirect_bypass_boot() {
  if (!_multilink_redirect_bypass_apply()) {
    return;
  }

  $drupal_static_user_access = &drupal_static('user_access', array());
  global $user;

  if (!isset($drupal_static_user_access[$user->uid])) {
    $drupal_static_user_access[$user->uid] = array();
  }

  $drupal_static_user_access[$user->uid]['bypass multilink redirect'] = TRUE;
  $drupal_static_user_access[$user->uid]['bypass multilink redirect silent'] = TRUE;
}

function _multilink_redirect_bypass_apply() {
  $current_ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ?
    $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
  $ips = variable_get('multilink_redirect_bypass_by_ip');
  if (in_array($current_ip, explode("\n", str_replace("\r", '', $ips)))) {
    return TRUE;
  }

  $current_user_agent = $_SERVER['HTTP_USER_AGENT'];
  $user_agents = variable_get('multilink_redirect_bypass_by_user_agent');
  if (in_array($current_user_agent, explode("\n", str_replace("\r", '', $user_agents)))) {
    return TRUE;
  }

  return FALSE;
}
